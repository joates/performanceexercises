package org.oateshome.exercises.services;

import android.app.Service;
import android.os.Binder;

/**
 * Created by joates on 11/2/13.
 */
public class ServiceBinder extends Binder {
    Service mService;

    public ServiceBinder(Service service){
        mService = service;
    }

    // This will give us a reference to our bound Service
    public Service getService(){
        return mService;
    }
}
