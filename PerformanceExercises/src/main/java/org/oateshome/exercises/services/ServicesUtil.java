package org.oateshome.exercises.services;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.Uri;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by joates on 10/30/13.
 */
public class ServicesUtil {
    public static final String PACKAGE = "org.oateshome.exercises";
    public static final String ACTION_SERVICE_COMPLETED = PACKAGE + ".action.ServiceCompleted";
    public static final int MAX_NUM_SERVICES = 1000; // This is an arbitrary maximum number.

    public static IntentFilter getCreateUriFilter(Uri uri){
        IntentFilter filter = new IntentFilter(ACTION_SERVICE_COMPLETED);
        filter.addDataScheme(uri.getScheme());
        filter.addDataAuthority(uri.getHost(), null);

        // Set the filter's pattern match to be an exact match using the Regex Pattern.LITERAL
        filter.addDataPath(uri.getPath(), Pattern.LITERAL);
        return filter;
    }

    public static boolean isServiceConnected(Activity activity, String serviceName) {
        ActivityManager manager = (ActivityManager)activity.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> services = manager.getRunningServices(MAX_NUM_SERVICES);

        // Look for the service in the list of currently running services
        for (ActivityManager.RunningServiceInfo serviceInfo : services){
            if (serviceInfo.getClass().getName().equals(serviceName)){
                return  true;
            }
        }
        return false;
    }

    public static int getGalleryCellWidth(Activity activity){
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        int width = (size.x / 2) - 10;
        return width;
    }
}
