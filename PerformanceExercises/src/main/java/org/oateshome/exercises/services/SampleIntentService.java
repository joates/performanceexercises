package org.oateshome.exercises.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;

/**
 * Created by joates on 10/29/13.
 */
public class SampleIntentService extends IntentService {

    public static final String SERVICE_NAME = "SampleIntentService";
    public static final String URI_SCHEME = "content";
    public static final String URI_AUTHORITY = "org.oateshome.exercises.services";

    public static final String SERVICE_WORK_SUCCESS_KEY = "ServiceWorkSuccess";


    public SampleIntentService() {
        super(SERVICE_NAME);
    }

    public Uri getUri(){
        Uri.Builder builder;
        builder = new Uri.Builder().scheme(URI_SCHEME).authority(URI_AUTHORITY).appendPath(SERVICE_NAME);
        return builder.build();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Do the work that will take a long time.
        // ...
        // ...
        // Since this is an IntentService, this is automatically done on a background thread.

        // Create an intent that contains the Action string, the URI representing the service, and
        // any needed information about how the task did its work, whether it succeeded, for example.
        Intent resultIntent = new Intent(ServicesUtil.ACTION_SERVICE_COMPLETED, getUri());
        resultIntent.setPackage(ServicesUtil.PACKAGE); // This will limit receivers to just our package
        resultIntent.putExtra(SERVICE_WORK_SUCCESS_KEY, true);

        // Once the work is finished, send a broadcast message to signal completion.
        // We use a sticky broadcast so the message will stick around in case the receiver that needs
        // it is not available when it arrives.
        getApplicationContext().sendStickyBroadcast(resultIntent);
    }
}
