package org.oateshome.exercises.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

/**
 * Created by joates on 11/2/13.
 */
public class SampleBoundService extends Service {
    Binder mBinder;
    boolean mIsWorkInProgress =false;

    public IBinder onBind(Intent intent) {
        if (mBinder == null){
            mBinder = new ServiceBinder(this);
        }

        // The Binder's job is just to hold a reference to the Service instance
        return mBinder;
    }

    // If a service is started with a call to startService() in Context, this method will be
    // where you start your background work
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        // Returning START_NOT_STICKY prevents Android from trying to resurrect the service after
        // a crash
        return START_NOT_STICKY;
    }

    public void methodThatStartsBackgroundWork(){
        // Check to see whether work is already in progress
        if (!mIsWorkInProgress){
            mIsWorkInProgress = true;
            Thread worker = new Thread(new Runnable() {
                @Override
                public void run() {
                    // Work happens here

                    // You need to stop the service when its work is finished.
                    SampleBoundService.this.stopSelf();
                }
            });

        }
    }
}
