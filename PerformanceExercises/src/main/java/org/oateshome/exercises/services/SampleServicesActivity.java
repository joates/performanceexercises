package org.oateshome.exercises.services;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;

/**
 * Created by jsoates on 10/30/13.
 */
public class SampleServicesActivity extends Activity implements ServiceConnection{

    boolean mIsBoundServiceConnected = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This service will do the background work
        SampleIntentService intentService = new SampleIntentService();

        // Create a filter that will listen for the particular broadcast we're interested in.
        IntentFilter filter = ServicesUtil.getCreateUriFilter(intentService.getUri());

        // This receiver could be registered from anywhere in the code. If you're launching a long-running
        // service, you'll often want it in an Activity or fragment other than the one that starts the service.
        registerReceiver(new SampleBroadcastReceiver(this), filter);

        // Launch the IntentService
        Intent startIntent = new Intent(this, SampleIntentService.class);
        startService(startIntent);

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Bind the bound Service creating it if necessary
        Intent startBoundIntent = new Intent(this, SampleBoundService.class);
        bindService(startBoundIntent, this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        // Whenever this Activity pauses unbind from the service. You will leak memory otherwise
        if (mIsBoundServiceConnected && ServicesUtil.isServiceConnected(this, SampleBoundService.class.getName())){
            unbindService(this);
        }
        super.onPause();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        mIsBoundServiceConnected = true;
        ServiceBinder binder = (ServiceBinder)iBinder;

        // Get a reference to the service from the Binder
        SampleBoundService boundService = (SampleBoundService)binder.getService();

        // start the work we want to have done
        boundService.methodThatStartsBackgroundWork();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        mIsBoundServiceConnected = false;
    }



    /**
     * Created by jsoates on 10/30/13.
     */
    public class SampleBroadcastReceiver extends BroadcastReceiver {

        Activity mActivity;

        public SampleBroadcastReceiver(Activity activity){
            mActivity = activity;
        }

        public void onReceive(Context context, Intent intent) {

            // Check for any errors or other details from the event that are relevant to your handling
            if (intent.getBooleanExtra(SampleIntentService.SERVICE_WORK_SUCCESS_KEY, false)){

                // Handle the event here by updating the UI, launching another process, etc...
            }

            // Be sure to clear the broadcast once you've handled it
            mActivity.removeStickyBroadcast(intent);
        }
    }
}