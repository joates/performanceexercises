package org.oateshome.exercises.asynctasks;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by joates on 10/26/13.
 */
public class AsyncTaskActivity extends FragmentActivity {
    public static final String ASYNC_TASK_FRAGMENT_TAG = "AsyncTaskFragment";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create the callback that should be called when the task finishes
        MyAsyncCallback callback = new MyAsyncCallback();

        // Call re-usable utility method for launching async tasks in a fragment with no UI
        AsyncTaskUtil.DoMyAsyncThing(this, ASYNC_TASK_FRAGMENT_TAG, callback);
    }

    private class MyAsyncCallback implements AsyncCallback{
        @Override
        public void doCallback() {
            // This is where we handle completion of the AsyncTask by updating the UI or whatever
            // else needs to be done.
        }
    }
}
