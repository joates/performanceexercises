package org.oateshome.exercises.asynctasks;

/**
 * Created by joates on 10/28/13.
 */
public interface AsyncCallback {
    public void doCallback();
}
