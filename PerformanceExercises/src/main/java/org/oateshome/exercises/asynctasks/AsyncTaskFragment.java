package org.oateshome.exercises.asynctasks;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * Created by jsoates on 10/28/13.
 */
public class AsyncTaskFragment extends Fragment {

    AsyncCallback mCallback;
    boolean mIsDoingWork = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // This ensures that the fragment and its task will survive device rotation
        setRetainInstance(true);
    }

    public void handleAsyncTask(){
        // Check to see whether the task is already in progress
        if (!mIsDoingWork){
            AsyncTask asyncTask = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    // Long running task here that is only relevant within this Activity.
                    // For example: pre-loading data from a web service call that may be needed later as
                    // the user interacts with the Activity.
                    return null;
                }

                @Override
                protected void onPostExecute(Object o) {
                    super.onPostExecute(o);
                    // Signal that the background work has finished
                    mIsDoingWork = false;

                    // What to do when the AsyncTask has finished
                    // For example: updating the UI with information produced by the long-running task.
                    if (mCallback != null){
                        mCallback.doCallback();
                    }
                }
            };

            // Signal that background work is now in progress
            mIsDoingWork = true;
            asyncTask.execute();
        }
    }

    public void setCallback(AsyncCallback callback){
        mCallback = callback;
    }
}