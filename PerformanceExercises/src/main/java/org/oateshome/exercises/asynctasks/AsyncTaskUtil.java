package org.oateshome.exercises.asynctasks;

import android.app.Activity;
import android.app.FragmentTransaction;

/**
 * Created by joates on 10/28/13.
 */
public class AsyncTaskUtil {
    public static void DoMyAsyncThing(Activity activity, String fragmentTag, AsyncCallback callback)
    {
        AsyncTaskFragment fragment = (AsyncTaskFragment)activity.getFragmentManager().findFragmentByTag(fragmentTag);
        if (fragment == null){
            fragment = new AsyncTaskFragment();
            FragmentTransaction transaction = activity.getFragmentManager().beginTransaction().disallowAddToBackStack();
            transaction.add(fragment, fragmentTag).commit();
        }
        fragment.setCallback(callback);
        fragment.handleAsyncTask();
    }
}
