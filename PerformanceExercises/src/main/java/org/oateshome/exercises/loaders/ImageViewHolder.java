package org.oateshome.exercises.loaders;

import android.widget.ImageView;
import android.widget.ToggleButton;

/**
* Created by sstream6 on 10/14/13.
*/
class ImageViewHolder {
    public ToggleButton highlight;
    public ImageView imageView;
}
