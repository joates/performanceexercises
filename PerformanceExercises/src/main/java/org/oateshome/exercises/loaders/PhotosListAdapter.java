package org.oateshome.exercises.loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import org.oateshome.exercises.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sstream6 on 9/24/13.
 */
public class PhotosListAdapter extends BaseAdapter {
    private List<Bitmap> mBitmapList = new ArrayList<Bitmap>();
    private LayoutInflater mInflater;
    private Context mContext;
    private Drawable mLoadingDrawable;

    public PhotosListAdapter(Context context, LayoutInflater inflater){
        mContext = context;
        mInflater = inflater;
        ProgressBar prog = new ProgressBar(context);
        mLoadingDrawable = prog.getIndeterminateDrawable();
    }

    public void add(Bitmap bitmap){
        mBitmapList.add(getCount(), bitmap);
    }

    @Override
    public int getCount() {
        return mBitmapList.size();
    }

    @Override
    public Object getItem(int i) {
        return mBitmapList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            view = mInflater.inflate(R.layout.photo_gallery_item, null);

            holder = new ViewHolder();
            holder.imageView = (ImageView)view.findViewById(R.id.photo_thumbnail);
            view.setTag(holder);
        }
        else{
            holder = (ViewHolder)view.getTag();
        }

        holder.imageView.setImageBitmap(mBitmapList.get(position));
        return view;
    }

    private class ViewHolder {
        public ImageView imageView;
    }
}
