package org.oateshome.exercises.loaders;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import org.oateshome.exercises.R;
import org.oateshome.exercises.services.ServicesUtil;

import java.util.ArrayList;

/**
* Created by sstream6 on 10/14/13.
*/
class ImageAdapter extends CursorAdapter {

    PhotoGalleryActivity mActivity;
    Drawable mLoadingDrawable;
    ImageHilightListener mListener = new ImageHilightListener();

    public ImageAdapter(Context context, Cursor cursor, int flags){
        super(context, cursor, flags);
        mActivity = (PhotoGalleryActivity)context;
        ProgressBar prog = new ProgressBar(context);
        mLoadingDrawable = prog.getIndeterminateDrawable();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view = LayoutInflater.from(context).inflate(R.layout.photo_gallery_item, null);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor){
        ImageViewHolder viewHolder = (ImageViewHolder)view.getTag();
        if (viewHolder == null) {
            viewHolder = new ImageViewHolder();
            viewHolder.highlight = (ToggleButton) view.findViewById(R.id.photo_highlight);
            viewHolder.highlight.setTextOff("");
            viewHolder.highlight.setTextOn("");
            viewHolder.imageView = (ImageView)view.findViewById(R.id.photo_thumbnail);
            int width = ServicesUtil.getGalleryCellWidth((Activity)context);
            viewHolder.imageView.getLayoutParams().width = width;
            viewHolder.imageView.getLayoutParams().height = width;
            viewHolder.highlight.getLayoutParams().width = width;
            viewHolder.highlight.getLayoutParams().height = width;
            view.setTag(viewHolder);
        }
        viewHolder.highlight.setOnCheckedChangeListener(mListener);
        int columnIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);
        String imageId = String.valueOf(cursor.getLong(columnIndex));
        String path = MediaStore.Images.Media.EXTERNAL_CONTENT_URI.buildUpon().appendPath(imageId).toString();

        // This one line is all we need to do for Picasso to handle loading an image from the file system asynchronously
        Picasso.with(mActivity).load(path).fit().placeholder(mLoadingDrawable).into(viewHolder.imageView);
        viewHolder.highlight.setTag(imageId);
        if (mActivity.mImageIds.contains(imageId)) {
            viewHolder.highlight.setChecked(true);
        } else {
            viewHolder.highlight.setChecked(false);
        }
    }

    private class ImageHilightListener implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView.getTag() != null) {
                String imageId = buttonView.getTag().toString();
                if (isChecked){
                    if (!mActivity.mImageIds.contains(imageId)){
                        mActivity.mImageIds.add(imageId);
                    }
                }
                else {
                    if (mActivity.mImageIds.contains(imageId)) {
                        mActivity.mImageIds.remove(imageId);
                    }
                }
            }
        }
    }
}
