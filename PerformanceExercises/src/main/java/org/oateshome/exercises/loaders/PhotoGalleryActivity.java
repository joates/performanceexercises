package org.oateshome.exercises.loaders;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Activity;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import org.oateshome.exercises.R;
import org.oateshome.exercises.services.ServicesUtil;

import java.util.ArrayList;

public class PhotoGalleryActivity extends Activity implements LoaderManager.LoaderCallbacks {

    public static final String ACTIVITY_ID = "org.oateshome.exercises.loaders.PhotoGalleryActivity";
    public static final String SELECTED_IMAGES_KEY = ACTIVITY_ID + ".SelectedImagesKey";
    public static final String ARE_IMAGES_SELECTED_KEY = ACTIVITY_ID + ".AreImagesSelected";
    private static final int IMAGE_CURSOR_LOADER_ID = 0;

    ArrayList<String> mImageIds = new ArrayList<String>();
    ImageAdapter mImageAdapter;
    GridView mGridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_gallery);
        mGridView = (GridView)findViewById(R.id.photo_grid);
        int width = ServicesUtil.getGalleryCellWidth(this);
        mGridView.setColumnWidth(width);

        Button selectBtn = (Button)findViewById(R.id.select_btn);
        selectBtn.setOnClickListener(new OnSelectImagesClickListnener(this));
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Initialize loader with an arbitrary unique id, an arguments Bundle, and an instance of
        // a class that implements the loader callbacks
        Bundle arguments = null;
        CursorLoader loader = (CursorLoader)getLoaderManager().initLoader(IMAGE_CURSOR_LOADER_ID, arguments, this);

        // Start loader immediately every time page loads.
        Cursor imageCursor = loader.loadInBackground();

        // Initialize adapter with the cursor.
        mImageAdapter = new ImageAdapter(this, imageCursor, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        // Set the adapter on the GridView.
        mGridView.setAdapter(mImageAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.photo_gallery, menu);
        return true;
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        // The ID lets us decide which loder to initialize, in case we're working with more than one.
        if (i == IMAGE_CURSOR_LOADER_ID) {
            String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};
            String orderBy = MediaStore.Images.Media.DATE_TAKEN + " DESC";

            // CursorLoader constructed with basic elements of a SQL query
            // * data source (URI)
            // * what columns to select (fields in table)
            // * what rows to select (formatted like a Where clause)
            // * values to insert for ?s in Where clause
            // * order by statement
            return new CursorLoader(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
        }
        return  null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object o) {
        System.out.println("Finished Image Load");
    }

    @Override
    public void onLoaderReset(Loader loader) {

    }

    private class OnSelectImagesClickListnener implements View.OnClickListener {
        PhotoGalleryActivity _activity;

        public OnSelectImagesClickListnener(PhotoGalleryActivity activity){
            _activity = activity;
        }

        public void onClick(View v){
            if (_activity.mImageIds.size() > 0){
                Intent intent = new Intent();
                intent.putExtra(ARE_IMAGES_SELECTED_KEY, true);
                intent.putStringArrayListExtra(SELECTED_IMAGES_KEY, _activity.mImageIds);

                _activity.setResult(RESULT_OK, intent);
                _activity.finish();
            }
            else {
                Toast.makeText(_activity, _activity.getResources().getString(R.string.no_photos_selected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
