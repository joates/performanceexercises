package org.oateshome.exercises.loaders;

import android.support.v4.app.LoaderManager;
import android.content.Intent;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.oateshome.exercises.R;

import java.util.ArrayList;

/**
 * A fragment to display photos stored by the application
 */
public class PhotoListFragment extends Fragment implements LoaderManager.LoaderCallbacks {

    public static final String FRAGMENT_TAG = "org.oateshome.exercises.loaders.PhotoListFragment";
    GridView mGrid;
    PhotosListAdapter mPhotosAdapter;
    private static final int IMPORTED_IMAGE_LOADER_ID = 0;

    @Override
    public void onCreate(Bundle savedInstanceState){
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.photo_list_layout, container, false);
        mGrid = (GridView) view.findViewById(R.id.photos_grid);
        mGrid.setEmptyView(view.findViewById(R.id.empty_view));

        // Make sure the grid and adapter are ready to receive image data.
        if (mPhotosAdapter == null){
            mPhotosAdapter = new PhotosListAdapter(getActivity(), getLayoutInflater(savedInstanceState));
        }
        mGrid.setAdapter(mPhotosAdapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.photo_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Launch our custom Photo Gallery.
        Intent intent = new Intent(getActivity(), PhotoGalleryActivity.class);
        startActivityForResult(intent, 0);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // When Android returns control to the Fragment that launched the activity, we retrieve the
        // list of image ids from the intent.
        if (data != null){
            if (data.getBooleanExtra(PhotoGalleryActivity.ARE_IMAGES_SELECTED_KEY, false)){
                getLoaderManager().initLoader(IMPORTED_IMAGE_LOADER_ID, data.getExtras(), this);
            }
        }
    }

    /* LoaderManager.LoaderCallbacks */
    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        Loader loader = null;
        switch (i) {
            case IMPORTED_IMAGE_LOADER_ID: {
                ArrayList<String> imageIds = bundle.getStringArrayList(PhotoGalleryActivity.SELECTED_IMAGES_KEY);
                loader = new ImportedImageLoader(getActivity(), imageIds, mPhotosAdapter);
            }
        }
        return loader;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader loader, Object o) {

    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader loader) {

    }

}