package org.oateshome.exercises.loaders;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.AsyncTaskLoader;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.Toast;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Jared Oates on 10/10/13.
 */
public class ImportedImageLoader extends AsyncTaskLoader {

    HashSet<String> mImagesToBeLoaded = new HashSet<String>();
    HashSet<String> mLoadedImages = new HashSet<String>();
    PhotosListAdapter mAdapter;
    Activity mActivity;

    // We use our own constructor to pass in parameters that are particular to our unique loader.
    public ImportedImageLoader(Activity activity, ArrayList<String> imageIds, PhotosListAdapter adapter) {
        super(activity);
        for (String id : imageIds) {
            mImagesToBeLoaded.add(id);
        }
        mAdapter = adapter;
        mActivity = activity;
    }

    @Override
    protected void onStartLoading() {
        // You might expect loadInBackground() to get called automatically from onStartLoading(),
        // but you need to call forceLoad() yourself.
        forceLoad();
    }

    @Override
    public Object loadInBackground() {
        // In this Loader, image loading happens all at once here in the background task
        for (String id : mImagesToBeLoaded) {
            if (mLoadedImages.contains(id)) continue;

            // Getting a bitmap from the file system can be a pretty resource intensive operation.
            // The way Android handles bitmaps in particular can use a lot of memory.
            // See http://developer.android.com/training/displaying-bitmaps/index.html
            String imgPath = GetImagePath(id);
            if (imgPath != null){
                // We use the image path to retrieve the image rather than just getting it directly from
                // the MediaStore with the URI so that we can scale with BitmapFactory.Options.
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                try{
                    final Bitmap bmp = BitmapFactory.decodeFile(imgPath, options);

                    // Once we have the bitmap, we add it to the adapter on the UI thread
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdapter.add(bmp);
                            mAdapter.notifyDataSetInvalidated();
                        }
                    });
                }
                catch (OutOfMemoryError e){
                    // If you load more than a few bitmaps, chances are good you'll wind up here
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getContext(), "Out of Memory Error", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }

        }
        return null;
    }

    String GetImagePath(String id) {
        //Start by getting the URI for the desired image.
        Uri resultUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
        ContentResolver resolver = getContext().getContentResolver();
        String[] fields = {MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA};
        String filename = "";

        Cursor imgCursor = MediaStore.Images.Media.query(resolver, resultUri, fields, null, null);
        if (imgCursor == null || imgCursor.getCount() <= 0){
            imgCursor.close();
            return filename;
        }
        imgCursor.moveToFirst();
        filename = imgCursor.getString(imgCursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
        imgCursor.close();

        //Check to see whether the returned filename has a protocol prepended and remove it if it's there
        Uri fileUri = Uri.parse(filename);
        if (fileUri.getScheme() != null) {
            filename = fileUri.getSchemeSpecificPart();
            filename = filename.substring(2);
        }
        return filename;
    }

    @Override
    public void deliverResult(Object data) {
        for (String path : mImagesToBeLoaded){
            mLoadedImages.add(path);
        }
        for (String path : mLoadedImages) {
            mImagesToBeLoaded.remove(path);
        }
        super.deliverResult(data);
    }
}
