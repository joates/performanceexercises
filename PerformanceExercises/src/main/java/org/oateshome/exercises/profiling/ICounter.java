package org.oateshome.exercises.profiling;

import java.io.InputStream;

/**
 * Created by sstream6 on 8/27/13.
 */
public interface ICounter {
    public int getNounCount();
    public int getVerbCount();
    public int getAdjectiveCount();
    public int getPrepositionCount();
    public int getUnknownCount();

    public void countWords(InputStream toBeCounted, InputStream nouns, InputStream verbs, InputStream adjectives, InputStream prepositions);
}
