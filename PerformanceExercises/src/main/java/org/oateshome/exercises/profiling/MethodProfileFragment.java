package org.oateshome.exercises.profiling;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import org.oateshome.exercises.loaders.PhotoGalleryActivity;
import org.oateshome.exercises.R;

import java.io.IOException;
import java.io.InputStream;

public class MethodProfileFragment extends Fragment {

    public static final String FRAGMENT_TAG = "org.oateshome.exercises.profiling.MethodProfileFragment";
    String VERB_FILE_NAME = "verblist.txt";
    String NOUN_FILE_NAME = "nounlist.txt";
    String ADJECTIVE_FILE_NAME = "adjectivelist.txt";
    String PREPOSITION_FILE_NAME = "prepositionlist.txt";
    String FROST_NIGHT_POEM = "frostnight.txt";
    String BEOWULF_POEM = "beowulf.txt";
    RadioButton beowulfRadio;
    RadioButton nightRadio;
    Button inefficientCountButton;
    TextView resultsTv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.method_profile_layout, container, false);
        beowulfRadio = (RadioButton)view.findViewById(R.id.beowulf_radio);
        nightRadio = (RadioButton)view.findViewById(R.id.night_radio);
        inefficientCountButton = (Button)view.findViewById(R.id.inefficientCountButton);
        inefficientCountButton.setOnClickListener(new CountClickListener());
        resultsTv = (TextView)view.findViewById(R.id.results_tv);


        return view;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.action_bar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.camera_btn)
        {
            Intent intent = new Intent(getActivity(), PhotoGalleryActivity.class);
            startActivityForResult(intent, 0);
        }
        return super.onOptionsItemSelected(item);
    }

    private class CountClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            countWordsInefficiently();

        }
    }

    private void countWordsInefficiently() {

        try {
            resultsTv.setText("");
            InputStream verbStream = getActivity().getAssets().open(VERB_FILE_NAME);
            InputStream nounStream = getActivity().getAssets().open(NOUN_FILE_NAME);
            InputStream adjStream = getActivity().getAssets().open(ADJECTIVE_FILE_NAME);
            InputStream prepStream = getActivity().getAssets().open(PREPOSITION_FILE_NAME);

            InputStream poemStream = getCurrentPoemStream();

            InefficientCounter inefficientCounter = new InefficientCounter();
            inefficientCounter.countWords(poemStream, nounStream, verbStream, adjStream, prepStream);

            String results = formatResults(getCurrentPoemName(), inefficientCounter);
            resultsTv.setText(results);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getCurrentPoemName() {
        if (nightRadio.isChecked()){
            return getResources().getString(R.string.acquainted_night);
        }
        if (beowulfRadio.isChecked()){
            return getResources().getString(R.string.beowulf);
        }
        return null;
    }
    private InputStream getCurrentPoemStream() {

        try {
            if (nightRadio.isChecked()){
                return getActivity().getAssets().open(FROST_NIGHT_POEM);
            }
            if (beowulfRadio.isChecked()){
                return getActivity().getAssets().open(BEOWULF_POEM);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  null;
    }

    private String formatResults(String poemName, ICounter counter){
        int nouns = counter.getNounCount();
        int verbs = counter.getVerbCount();
        int adjs = counter.getAdjectiveCount();
        int preps = counter.getPrepositionCount();
        int unkn = counter.getUnknownCount();

        String result = getResources().getString(R.string.result_string, poemName, nouns, verbs, adjs, preps, unkn);
        return result;
    }
}
