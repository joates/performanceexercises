package org.oateshome.exercises.profiling;

import org.oateshome.exercises.profiling.ICounter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by sstream6 on 8/22/13.
 */
public class InefficientCounter implements ICounter {

    private int mNounCount = 0;
    private int mVerbCount = 0;
    private int mAdjectiveCount = 0;
    private int mPrepositionCount = 0;
    private int mUnknownCount = 0;

    public int getNounCount(){
        return mNounCount;
    }
    public int getVerbCount(){
        return mVerbCount;
    }

    public int getAdjectiveCount() {
        return mAdjectiveCount;
    }

    public int getUnknownCount() {
        return mUnknownCount;
    }

    public int getPrepositionCount() {
        return mPrepositionCount;
    }

    public void countWords(InputStream toBeCounted, InputStream nouns, InputStream verbs, InputStream adjectives, InputStream prepositions)
    {
        ArrayList<String> toBeCountedList = toArrayList(toBeCounted);
        ArrayList<String> nounsList = toArrayList(nouns);
        ArrayList<String> verbsList = toArrayList(verbs);
        ArrayList<String> adjectivesList = toArrayList(adjectives);
        ArrayList<String> prepositionsList = toArrayList(prepositions);

        for (String word : toBeCountedList){
            boolean noun = containsWord(nounsList, word);
            boolean verb = containsWord(verbsList, word);
            boolean adj = containsWord(adjectivesList, word);
            boolean prep = containsWord(prepositionsList, word);

            if (noun){
                mNounCount += 1;
            }
            if (verb){
                mVerbCount += 1;
            }
            if (adj){
                mAdjectiveCount += 1;
            }
            if (prep){
                mPrepositionCount += 1;
            }
            if (!(noun || verb || adj || prep)){
                mUnknownCount += 1;
                System.out.println(word);
            }
        }
    }

    private boolean containsWord(ArrayList<String> list, String word)
    {
        for (String listword : list)
        {
            if (word.equalsIgnoreCase(listword))
            {
                return true;
            }
        }
        return false;
    }

    private ArrayList<String> toArrayList(InputStream stream)
    {
        ArrayList<String> returnList = new ArrayList<String>();
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader bufferedReader = new BufferedReader(reader);

        String currentLine;
        try {
            while ((currentLine = bufferedReader.readLine()) != null){
                returnList.add(currentLine);
            }
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnList;
    }
}
