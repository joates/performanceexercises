package org.oateshome.exercises;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import org.oateshome.exercises.loaders.PhotoListFragment;
import org.oateshome.exercises.profiling.MethodProfileFragment;

/**
 * An activity representing a single Exercise detail screen. This
 * activity is only used on handset devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ExerciseListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing
 * more than a {@link ExerciseDetailFragment}.
 */
public class ExerciseDetailActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail);

        // Show the Up button in the action bar.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            String tag = getIntent().getStringExtra(ExerciseDetailFragment.ARG_ITEM_ID);
            if (tag.equals(MethodProfileFragment.FRAGMENT_TAG)) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(MethodProfileFragment.FRAGMENT_TAG);
                if (fragment == null){
                    fragment = new MethodProfileFragment();
                }
                replaceFragment(fragment, MethodProfileFragment.FRAGMENT_TAG);
            }
            else if (tag.equals(PhotoListFragment.FRAGMENT_TAG)){
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(PhotoListFragment.FRAGMENT_TAG);
                if (fragment == null){
                    fragment = new PhotoListFragment();
                }
                replaceFragment(fragment, PhotoListFragment.FRAGMENT_TAG);
            }
        }
    }

    private void replaceFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.exercise_detail_container, fragment, tag)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. Use NavUtils to allow users
                // to navigate up one level in the application structure. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
                //
                NavUtils.navigateUpTo(this, new Intent(this, ExerciseListActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
