package org.oateshome.exercises.data;

import org.oateshome.exercises.loaders.PhotoListFragment;
import org.oateshome.exercises.profiling.MethodProfileFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class Exercises {

    /**
     * An array of sample (dummy) items.
     */
    public static List<ExerciseItem> ITEMS = new ArrayList<ExerciseItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, ExerciseItem> ITEM_MAP = new HashMap<String, ExerciseItem>();

    static {
        // Add 3 sample items.
        addItem(new ExerciseItem(MethodProfileFragment.FRAGMENT_TAG, "Method Profile Exercise"));
        addItem(new ExerciseItem(PhotoListFragment.FRAGMENT_TAG, "Loader Exercise"));
    }

    private static void addItem(ExerciseItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class ExerciseItem {
        public String id;
        public String content;

        public ExerciseItem(String id, String content) {
            this.id = id;
            this.content = content;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
