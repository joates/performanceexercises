package org.oateshome.exercises;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;

import org.oateshome.exercises.loaders.PhotoListFragment;
import org.oateshome.exercises.profiling.MethodProfileFragment;


/**
 * An activity representing a list of Exercises. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ExerciseDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ExerciseListFragment} and the item details
 * (if present) is a {@link ExerciseDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link ExerciseListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class ExerciseListActivity extends FragmentActivity
        implements ExerciseListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);

        if (findViewById(R.id.exercise_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((ExerciseListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.exercise_list))
                    .setActivateOnItemClick(true);
        }


        ViewServer.get(this).addWindow(this);
    }

    /**
     * Callback method from {@link ExerciseListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String tag) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.

            if (tag == MethodProfileFragment.FRAGMENT_TAG) {
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(MethodProfileFragment.FRAGMENT_TAG);
                if (fragment == null){
                    fragment = new MethodProfileFragment();
                }
                replaceFragment(fragment, MethodProfileFragment.FRAGMENT_TAG);
            }
            else if (tag == PhotoListFragment.FRAGMENT_TAG){
                Fragment fragment = getSupportFragmentManager().findFragmentByTag(PhotoListFragment.FRAGMENT_TAG);
                if (fragment == null){
                    fragment = new PhotoListFragment();
                }
                replaceFragment(fragment, PhotoListFragment.FRAGMENT_TAG);
            }
            else {
                Bundle arguments = new Bundle();
                arguments.putString(ExerciseDetailFragment.ARG_ITEM_ID, tag);
                Fragment fragment = new ExerciseDetailFragment();
                fragment.setArguments(arguments);
                replaceFragment(fragment, ExerciseDetailFragment.FRAGMENT_TAG);
            }
        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, ExerciseDetailActivity.class);
            detailIntent.putExtra(ExerciseDetailFragment.ARG_ITEM_ID, tag);
            startActivity(detailIntent);
        }
    }

    private void replaceFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.exercise_detail_container, fragment, tag)
            .commit();
    }
}
